default:
  image: node:latest

cache:
  key: $CI_MERGE_REQUEST_ID
  paths:
    - node_modules/

stages:
  - init
  - cleanup
  - test
  - build
  - pre-release
  - release
  - post-release

init:
  stage: init
  script:
    - echo "Testing ENVARs are exported..."
    - ./.gitlab/pipeline_tests/test-envar-exports.sh
    - echo "Exporting ENVARs..."
    - ./.gitlab/pipeline_utils/envars.sh
    - echo "Installing dependencies..."
    - npm ci
  artifacts:
    expire_in: 1h
    reports:
      dotenv: vars.env
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

manage-issues-on-mr:
  stage: init
  image: mvdan/shfmt:latest-alpine
  before_script:
    - apk update
    - apk add bash jq curl
  script:
    - echo "Managing Issues..."
    - ./.gitlab/pipeline_utils/manage-issues-on-mr.sh
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

manage-issues-on-merge:
  stage: init
  image: mvdan/shfmt:latest-alpine
  before_script:
    - apk update
    - apk add bash jq curl
  script:
    - echo "Managing Issues..."
    - bash -x ./.gitlab/pipeline_utils/manage-issues-on-merge.sh
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Only run when merging to dev
    # This will run on all pushes to `dev`, however there is logic in the
    # script to ensure it only runs if it's related to an MR.
    # This is because GitLab CI doesn't expose the MR variables when merging
    # from an MR for some reason
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

delete-packages-dev:
  stage: cleanup
  script:
    - apt-get update
    - apt-get install jq --yes
    - echo "Deleting old dev releases... "
    - ./.gitlab/pipeline_utils/cleanup.sh
  needs:
    - job: init
      artifacts: true
  rules:
    - if: $CPR_SCHEDULE == "cleanup-dev"

lint-code:
  stage: test
  script:
    - echo "Linting code..."
    - npm run lint
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

code-formatting:
  stage: test
  script:
    - >
      npx prettier
      --check
      gulpfile.mjs
      gulp/*.mjs
      src/*.js
      src/modules
      src/**/*.yaml
      src/*.json
      src/**/*.json
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

lint-json:
  stage: test
  script:
    - echo "Linting json..."
    - ./.gitlab/pipeline_tests/test-json.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

lint-yaml:
  stage: test
  script:
    - apt-get update
    - apt-get install yamllint --yes
    - echo "Linting yaml..."
    - ./.gitlab/pipeline_tests/test-yaml.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

validate-packs:
  stage: test
  before_script:
    - apt-get update
    - apt-get install jq --yes
  script:
    - echo "Validating pack fragments..."
    - ./.gitlab/pipeline_tests/test-packs.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == "dev"
    # Run on git tag
    - if: $CI_COMMIT_TAG

lint-markdown:
  stage: test
  script:
    - echo "Linting markdown..."
    - ./.gitlab/pipeline_tests/test-markdown.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

unit-test:
  stage: test
  script:
    - echo "Executing tests..."
    - npm test
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

test-shell-scripts:
  stage: test
  image: mvdan/shfmt:latest-alpine
  before_script:
    - apk update
    - apk add bash shellcheck
  script:
    - echo "Testing Shell Scripts..."
    - ./.gitlab/pipeline_tests/test-shell-scripts.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

test-less-colors:
  stage: test
  script:
    - echo "Testing CSS for non-variable color usage..."
    - ./.gitlab/pipeline_tests/test-less-colors.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

test-less-units:
  stage: test
  script:
    - echo "Testing CSS for non-relative unit usage..."
    - ./.gitlab/pipeline_tests/test-less-units.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

language-babele:
  stage: test
  script:
    - echo "Checking Babele exports..."
    - npx gulp generateBabele
    - ./.gitlab/pipeline_tests/test-lang-babele.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

language-unused-strings:
  stage: test
  script:
    - echo "Checking language file for unused strings..."
    - ./.gitlab/pipeline_tests/test-lang-file.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

language-exists:
  stage: test
  before_script:
    - apt-get update
    - apt-get install jq --yes
  script:
    - echo "Checking if all the language files specified in system.json exist..."
    # disabling while we fix Crowdin
    # -./.gitlab/pipeline_tests/test-lang-existence.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

language-strings:
  stage: test
  before_script:
    - apt-get update
    - apt-get install jq --yes
  script:
    - echo "Checking if translation strings exist where they should..."
    # disabling while we fix Crowdin
    # - ./.gitlab/pipeline_tests/test-lang-strings.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

changelog-has-changed:
  stage: test
  image: mvdan/shfmt:latest-alpine
  before_script:
    - apk update
    - apk add bash jq curl git
  script:
    - echo "Checking if the CHANGELOG.md changed..."
    - ./.gitlab/pipeline_tests/test-changelog-dev-changed.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

handelbars-inlinestyles:
  stage: test
  script:
    - echo "Checking if we are using inline styles..."
    - ./.gitlab/pipeline_tests/test-handlebars-inelinestyles.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handelbars-attributes:
  stage: test
  script:
    - echo "Checking if HTML attributes are quoted correctly..."
    - ./.gitlab/pipeline_tests/test-handlebars-attributes.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-tooltips:
  stage: test
  script:
    - echo "Checking if we are using 'data-toolip' for tooltips..."
    - ./.gitlab/pipeline_tests/test-handlebars-tooltips.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-traces:
  stage: test
  script:
    - echo "Checking if there are proper trace statements in all hbs files..."
    - ./.gitlab/pipeline_tests/test-handlebars-trace.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-tags:
  stage: test
  script:
    - echo "Checking if there are matched html tags in all hbs files..."
    - ./.gitlab/pipeline_tests/test-handlebars-tags.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-compare:
  stage: test
  script:
    - echo "Checking useage of cprCompare to test booleans ..."
    - ./.gitlab/pipeline_tests/test-handlebars-compare.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-helpers:
  stage: test
  script:
    - echo "Checking use of handlebars helpers..."
    - ./.gitlab/pipeline_tests/test-handlebars-helpers.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

handlebars-templates:
  stage: test
  script:
    - echo "Checking use of handlebars templates..."
    - ./.gitlab/pipeline_tests/test-handlebars-templates.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

code-localization:
  stage: test
  script:
    - echo "Checking if our own localization and notification code is used..."
    - ./.gitlab/pipeline_tests/test-misc.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

game-id:
  stage: test
  script:
    - echo "Making sure game.system.id is used appropriately..."
    - ./.gitlab/pipeline_tests/test-game-ids.sh
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

compendia-use:
  stage: test
  script:
    - echo "Making sure compendia APIs are used..."
    - ./.gitlab/pipeline_tests/test-compendia-use.sh
  needs:
    - job: init
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Run on merge to 'dev'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Build without artifacts when not tagging a release
test-build:
  stage: test
  script:
    - echo "Building cyberpunk-red-core..."
    - npm run build
  needs:
    - job: init
      artifacts: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

# Builds and Uploads artifacts the gitlab generic repo
build-artifacts:
  stage: build
  before_script:
    - apt-get update
    - apt-get install curl zip jq -y
  script:
    - echo "Building cyberpunk-red-core..."
    - ./.gitlab/pipeline_utils/build.sh
  needs:
    - job: init
      artifacts: true
    - job: code-formatting
    - job: code-localization
    - job: compendia-use
    - job: game-id
    - job: test-less-units
    - job: test-less-colors
    - job: handlebars-traces
    - job: handlebars-tags
    - job: handelbars-attributes
    - job: handelbars-inlinestyles
    - job: handlebars-helpers
    - job: handlebars-compare
    - job: handlebars-tooltips
    - job: language-babele
    - job: language-unused-strings
    - job: language-exists
    - job: lint-code
    - job: lint-json
    - job: lint-yaml
    - job: lint-markdown
    - job: test-build
      optional: true
    - job: test-shell-scripts
      optional: true
    - job: unit-test
    - job: validate-packs
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Creates a GitLab release with the previously uploaded artifacts
create-release:
  stage: build
  before_script:
    - apt-get update
    - apt-get install curl jq -y
    - curl --silent --location --output /usr/local/bin/release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64"
    - chmod +x /usr/local/bin/release-cli
  script:
    - echo "Running Release Job..."
    - ./.gitlab/pipeline_utils/release.sh
  needs:
    - job: init
      artifacts: true
    - job: build-artifacts
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Pre-release tests
pre-release:
  stage: pre-release
  before_script:
    - apt-get update
    - apt-get install curl zip jq -y
  script:
    - echo "Running Pre Release Checks"
    - ./.gitlab/pipeline_tests/pre-release-checks.sh
  needs:
    - job: init
      artifacts: true
    - job: build-artifacts
    - job: create-release
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Build the `latest` system.json and upload to gitlab generic repo
# This makes the release available in Foundry
publish:
  stage: release
  before_script:
    - apt-get update
    - apt-get install curl jq -y
  script:
    - echo "Publishing cyberpunk-red-core..."
    - ./.gitlab/pipeline_utils/publish.sh
  needs:
    - job: init
      artifacts: true
    - job: build-artifacts
    - job: pre-release
      optional: true
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on MRs
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Builds system to generate a release announcment then webooks to Discord
announce:
  stage: post-release
  before_script:
    - apt-get update
    - apt-get install curl jq -y
  script:
    - echo "Generating message from changelog..."
    - npx run build
    - echo "Announcing the new release on Discord..."
    - ./.gitlab/pipeline_utils/discord-release.sh
  needs:
    - job: init
      artifacts: true
    - job: build-artifacts
    - job: publish
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on git tag
    - if: $CI_COMMIT_TAG

# Close all Issues included in a release
manage-issues-on-release:
  stage: release
  image: mvdan/shfmt:latest-alpine
  before_script:
    - apk update
    - apk add bash jq curl
  script:
    - echo "Managing Issues..."
    - ./.gitlab/pipeline_utils/manage-issues-on-release.sh
  needs:
    - job: init
      artifacts: true
    - job: create-release
  rules:
    # Don't run if we're in the cleanup-dev schedule
    - if: $CPR_SCHEDULE == "cleanup-dev"
      when: never
    # Run on git tag
    - if: $CI_COMMIT_TAG
